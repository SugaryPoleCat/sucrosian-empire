// this will collect all objects.
require('pg').defaults.parseInt8 = true;
const sequelize = require('sequelize');
try {
    let database;
    if (process.argv.includes('-dev')) {
        database = new sequelize(process.env.DEV_DB_NAME, process.env.DEV_DB_USER, process.env.DEV_DB_PASS, {
            host: 'localhost',
            dialect: 'sqlite',
            logging: true,
            storage: `${process.env.DEV_DB_NAME}.sqlite`,
        });
    } else if (process.argv.includes('-pub')) {
        database = new sequelize(`postgres://${process.env.PUB_DB_USER}:${process.env.PUB_DB_PASS}@localhost/${process.env.PUB_DB_NAME}?sslmode=disable`);
    }

    // FIND A WAY TO GET THIS LOADED DYNAMICALLY

    // BANKS
    const Bank = database.import('./models/Banking/Bank.js');
    database.import('./models/Banking/BankUser.js');

    // USERS
    database.import('./models/User/User.js');
    database.import('./models/User/UserCurrencies.js');
    database.import('./models/User/UserCurrenciesEaten.js');
    database.import('./models/User/UserRoles.js');

    const force = process.argv.includes('--force') || process.argv.includes('-f');
    database.sync({
            force
        })
        .then(async () => {
            const banks = [
                await Bank.upsert({
                    name: 'Muffin Cakes',
                    initialFee: 1500,
                    interestRate: 2000,
                    security: 20,
                    monthlyFee: 10
                }),
            ];
            await Promise.all(banks);
        }).catch((err) => {
            console.error(err);
        }).finally(() => {
            console.log('Database synced');
            database.close();
        });
} catch (err) {
    throw new Error(err);
}