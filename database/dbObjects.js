// this will collect all objects.
require('pg').defaults.parseInt8 = true;
const sequelize = require('sequelize');
try {
    let database;
    if (process.argv.includes('-dev')) {
        database = new sequelize(process.env.DEV_DB_NAME, process.env.DEV_DB_USER, process.env.DEV_DB_PASS, {
            host: 'localhost',
            dialect: 'sqlite',
            logging: true,
            storage: `${process.env.DEV_DB_NAME}.sqlite`,
        });
    } else if (process.argv.includes('-pub')) {
        database = new sequelize(`postgres://${process.env.PUB_DB_USER}:${process.env.PUB_DB_PASS}@localhost/${process.env.PUB_DB_NAME}?sslmode=disable`);
    }

    // FIND A WAY TO GET THIS LOADED DYNAMICALLY
    // BANKS
    const Bank = database.import('./models/Banking/Bank.js');
    const BankUser = database.import('./models/Banking/BankUser.js');

    // USERS
    const User = database.import('./models/User/User.js');
    const UserRoles = database.import('./models/User/UserRoles.js');
    const UserCurrencies = database.import('./models/User/UserCurrencies.js');
    const UserCurrenciesEaten = database.import('./models/User/UserCurrenciesEaten.js');

    module.exports = {
        Bank,
        BankUser,
        User,
        UserRoles,
        UserCurrencies,
        UserCurrenciesEaten,
    };
} catch (err) {
    throw new Error(err);
}