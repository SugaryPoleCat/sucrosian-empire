require('pg').defaults.parseInt8 = true;
const sequelize = require('sequelize');
let database;
if(process.argv.includes('-dev')){
    database = new sequelize(process.env.DEV_DB_NAME, process.env.DEV_DB_USER, process.env.DEV_DB_PASS, {
        host: 'localhost',
        dialect: 'sqlite',
        logging: true,
        storage: `${process.env.DEV_DB_NAME}.sqlite`,
    });
}
else if(process.argv.includes('-pub')){
    database = new sequelize(`postgres://${process.env.PUB_DB_USER}:${process.env.PUB_DB_PASS}@localhost/${process.env.PUB_DB_NAME}?sslmode=disable`);
}

// if(process.env.FLAGS);

database.import('../models/User/User.js');
database.import('../models/User/UserCurrencies.js');
database.import('../models/User/UserCurrenciesEaten.js');
database.import('../models/User/UserRoles.js');

const force = process.argv.includes('--force') || process.argv.includes('-f');

database.sync({ force }).then(async () => {
    // await Promise.all();
    // this is only needed if we want stuff in them at the ready.
    database.close();
}).catch(console.error).finally(() => {
    database.close();
});