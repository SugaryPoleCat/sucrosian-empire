// THIS just keeps track of who has a bank.
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('bank', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        initialFee: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        interestRate: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        // Unless we plan on adding some stupid like, command to steal from others, not needd.
        security: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        // Monthly fee isnt needed maybe
        monthlyFee: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['id'],
        }],
        timestamps: false,
    });
};
