// THIS just keeps track of who has a bank.
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('bankUser', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        userID: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true,
        },
        bankID: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        strawberrycubes: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrosium: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrons: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        indexes: [{
            unique: true,
            fields: ['id', 'userID', 'bankID'],
        }],
        timestamps: false,
    });
};
