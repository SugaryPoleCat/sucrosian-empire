module.exports = (sequelize, DataTypes) => {
    return sequelize.define('bot_servers', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        serverID:{
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        adminID: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        channelAnnounce: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    },
    {
        indexes: [{
            unique: true,
            fields: ['id', 'serverID'],
        }],
        timestamps: false,
    });
};