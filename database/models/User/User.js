module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        userID: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        displayName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        userRole: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        indexes: [{
            unique: true,
            fields: ['id', 'userID'],
        }],
        timestamps: false,
    });
};