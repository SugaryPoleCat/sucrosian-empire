module.exports = (sequelize, DataTypes) => {
    return sequelize.define('userCurrencies', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        userID: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        cubes: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrosium: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
        sucrons: {
            type: DataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0,
        },
    },
    {
        indexes: [{
            unique: true,
            fields: ['id', 'userID'],
        }],
        timestamps: false,
    });
};