module.exports = (sequelize, DataTypes) => {
    return sequelize.define('userRoles', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        privilage: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    },
    {
        indexes: [{
            unique: true,
            fields: ['id', 'privilage'],
        }],
        timestamps: false,
    });
};