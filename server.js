const express = require('express');
const path = require('path');
const server = express();
const exphbs = require('express-handlebars');
const session = require('express-session');
let app;

async function load(){
    try{
        switch(process.env.FLAGS){
            case '-pub':
                process.env.NODE_ENV === await 'production';
                break;
            case '-dev':
                process.env.NODE_ENV === await 'development';
                process.env.DEBUG === 'express:* node server.js';
                break;
        }
        console.log(path.join(__dirname, 'src/views/'));
        await server.set('views', './src/views/');

        const hbs = await exphbs.create({
            // layoutsDir: [
            //     './src/views/',
            // ],
            partialsDir: [
                './src/views/partials/',
            ],
        });
        await server.engine('handlebars', hbs.engine);
        await server.set('view engine', 'handlebars');

        await server.use(express.static('./src/public'));
        await server.use(express.json());
        await server.use(express.urlencoded({ extended: true }));

        await server.use(session({
            // cookie: { maxAge: SESSION_EXPIRE },
            // store: new MemoryStore({
            //     checkPeriod: SESSION_EXPIRE,
            // }),
            // secret: SESSION_SECRET,
            cookie: { 
                maxAge: 86400000,
                secure: true, 
            },
            key: process.env.SESSION_KEY,
            secret: process.env.SESSION_SECRET,
            resave: false,
            saveUninitialized: false,
        }));
        app = await require('./src/app');
    }
    catch(err){
        throw new Error(err);
    }
}
async function run(flags){
        process.env.FLAGS === await flags;
        await load();
        await app.run(server);
        server.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}!`));
}
run(process.argv[2]);

process.on('unhandledRejection', error => {
    console.error(error);
    process.exit();
});