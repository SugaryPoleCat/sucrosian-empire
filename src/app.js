// const express = require('express');
// const app = express();
async function run(app){
    app.use('/', require('./routes/index'));
    app.use('/babe', require('./routes/babe'));
    app.use('/links', require('./routes/links'));
    app.use('/event', require('./routes/events'));
    app.use('/story', require('./routes/story'));
    app.use('/test', require('./routes/test'));

    app.use((req, res, next)=>{
        res.status(404);
        if(req.accepts('html')){
            res.render('404', { url: req.url.substr(1) });
            return;
        }
        if(req.accepts('json')){
            res.send({error: 'page not found'});
            return;
        }
        res.type('txt').send('page not found');
    });
}

module.exports = {
    run,
};