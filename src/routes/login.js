const express = require('express');
const router = express.Router();
try{
    router.get('/', async function(req, res, next){
        let loggedIn;
        if(req.session.loggedIn)
            loggedIn = req.session.loggedIn;
        res.render('login', {
            loggedIn: loggedIn,
            title: 'Login',
        });
    });
    module.exports = router;
}
catch(err){
    return console.error(err);
}