const express = require('express');
const router = express.Router();
const { Bank } = require('../../database/dbObjects');
try{
    async function asd(id){
        const bank = Bank.findOne({
            where: { id: parseInt(id) },
        });
        return bank;
    }
    router.get('/', async function(req, res, next){
        const bank = await asd(1);
        res.render('test', {
            bank: bank.name,
        });
    });
    module.exports = router;
}
catch(err){
    console.error(err);
}